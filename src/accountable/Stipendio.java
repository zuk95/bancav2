package accountable;

public class Stipendio extends AbstractAccountable {
    private double importo;

    public Stipendio(double importo) {
        this.type=AccountableType.ACCREDITO;
        this.importo = importo;
    }

    @Override
    public double getImporto() {
        return importo;
    }

    @Override
    public AccountableType getType() {
        return super.getType();
    }
}
