package accountable;

public class AbbonamentoSky extends AbstractAccountable{
    private double importo;

    public AbbonamentoSky(double importo) {
        this.type=AccountableType.ADDEBITO;
        this.importo = importo;
    }

    @Override
    public double getImporto() {
        return importo;
    }

    @Override
    public AccountableType getType() {
        return super.getType();
    }
}
