package accountable;

public interface Accountable {
    double getImporto();
    AccountableType getType();
}
