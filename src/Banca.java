import accountable.Accountable;
import conti.*;

import java.util.LinkedHashMap;
import java.util.Map;

public class Banca {
    private String nome;
    private Map<String, Conto> conti;

    public Banca(String nome) {
        this.nome = nome;
        this.conti=new LinkedHashMap<>();
    }

public Conto aggiungiConto(String cf,ContoType type){
        Conto c = null;
    switch (type) {
        case CORRENTE:
            c = new ContoCorrente(cf,genIban());
            break;
        case WEB:
            c = new ContoCorrenteWeb(cf,genIban());
            break;
        case DEPOSITO:
            c = new ContoDeposito(cf,genIban());
            break;
    }
    conti.put(c.getIban(), c);
    return c;
}

public boolean operazione(String iban, double importo){
        Conto c= conti.get(iban);
        return c.operazione(importo);
        }

        public boolean addAccountable(String iban, Accountable acc){
        return conti.get(iban).addAccountable(acc);
        }

        public boolean fineMese(){
        conti.forEach((k,v)->v.fineMese());
        return true;
        }

        public boolean logIn(String iban,String pass){
        Conto c = conti.get(iban);
        if(c instanceof ContoCorrenteWeb){
            ContoCorrenteWeb web = (ContoCorrenteWeb)c;
            return web.login(pass);
        }else{
        return false;}
        }

        public boolean changePassword(String iban,String oldPass, String newPass){
        Conto c = conti.get(iban);
        if(c instanceof ContoCorrenteWeb){
            ContoCorrenteWeb web= (ContoCorrenteWeb)c;
            return web.setPassword(oldPass,newPass);
        }else{
        return false;}
        }

    public String toString() {
        String s = "\nBanca " + nome + "\n";
        s += "------------\n";
        s += (conti==null?0:conti.size()) + " conti attivi\n";
        s += "------------\n";
        for (Conto c:conti.values()) {
            s += c + "\n";
        }
        s += "------------\n";
        return s + "\n";
    }

    private String genIban() {
        return nome+conti.size();
    }



}
