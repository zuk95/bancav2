package conti;

import accountable.Accountable;
import accountable.AccountableType;

import java.util.ArrayList;

public abstract class AbstractConto implements Conto {
    private String cf;
    private String iban;
    private double saldo;
    ArrayList<Accountable> accountables;

    public AbstractConto(String cf, String iban) {
        this.cf = cf;
        this.iban = iban;
        this.saldo=0;
        this.accountables= new ArrayList<>();
    }

    @Override
    public String getIban() {
        return iban;
    }

    @Override
    public double getSaldo() {
        return saldo;
    }

    private boolean prelievo(double importo){
        if(-importo > saldo){
            return false;
        }else{
            saldo=saldo+importo;
            return true;
        }
    }

    private boolean deposito(double importo){
        if(importo>0){
            saldo=saldo+importo;
            return true;
        }else{
            return false;
        }
    }

    public boolean operazione(double importo){
        if(importo>0){
            this.deposito(importo);
            return true;
        }else if(importo<0){
            this.prelievo(importo);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean addAccountable(Accountable acc) {
        accountables.add(acc);
        return true;
    }

    @Override
    public boolean fineMese() {
        for (Accountable acconti:accountables
             ) {
            switch(acconti.getType()){
                case ACCREDITO:
                    saldo=saldo+acconti.getImporto();
                    break;
                case ADDEBITO:
                    if(saldo>=acconti.getImporto()){
                        saldo=saldo-acconti.getImporto();
                    }else{
                        return false;
                    }
                    break;
            }
        }
        return true;
    }

    public String toString() {
        return "iban: " + iban + ";" + "\tintestatario: " + cf
                + ";" + "\tsaldo: " + saldo;
    }

}
