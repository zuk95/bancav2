package conti;

public class ContoDeposito extends AbstractConto{

    public ContoDeposito(String cf, String iban) {
        super(cf, iban);
    }

    @Override
    public boolean operazione(double importo) {
        if (importo > 0) {
            super.operazione(importo);
            return true;
        } else {
            return false;
        }
    }
}
