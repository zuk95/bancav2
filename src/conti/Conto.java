package conti;

import accountable.Accountable;

public interface Conto {
    boolean operazione(double importo);
    String getIban();
    double getSaldo();
    boolean fineMese();
    boolean addAccountable(Accountable acc);
}
