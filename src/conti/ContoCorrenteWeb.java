package conti;

public class ContoCorrenteWeb extends ContoCorrente{
    private boolean loggedIn=false;
        private boolean firstLogin;
    private String password;


    public ContoCorrenteWeb(String cf, String iban) {
        super(cf, iban);
        this.password="changeme";
        this.firstLogin=true;
    }

    public boolean login(String pass){
        if(pass.equals(password) && !firstLogin){
            loggedIn=true;
            return loggedIn;
        }else{
            return false;
        }
    }

    public boolean setPassword(String oldpass,String newPass){
        if(oldpass.equals(password)){
            password=newPass;
            firstLogin=false;
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean operazione(double importo) {
        if(!loggedIn && firstLogin){
            return false;
        }else{
           return super.operazione(importo);
        }
    }
}
